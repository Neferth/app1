Rails.application.routes.draw do
 
  root :to => "invoice_headers#index"

  resources :invoice_lines
  resources :invoice_headers
  match 'invoice_lines/new/:invoice_header_id', to: 'invoice_lines#new', via: [:get]
  
end
