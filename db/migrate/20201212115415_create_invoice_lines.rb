class CreateInvoiceLines < ActiveRecord::Migration[5.0]
  def change
    create_table :invoice_lines do |t|
      t.text :name
      t.integer :quantity, null: false
      t.decimal :unit_price,  null: false
      t.string :color
      t.decimal :line_total
      t.references :invoice_header, foreign_key: true

      t.timestamps
    end
  end
end
