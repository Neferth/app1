class CreateInvoiceHeaders < ActiveRecord::Migration[5.0]
  def change
    create_table :invoice_headers do |t|
      t.text :name, null: false
      t.text :lastname, null: false
      t.string :dni, null: false
      t.text :adress
      t.string :phone
      t.string :email
      t.decimal :tax
      t.decimal :total, null: false

      t.timestamps
    end
  end
end
