class AddDefaultTotalToInvoiceHeader < ActiveRecord::Migration[5.0]
  def change
  
      change_column :invoice_headers, :tax, :decimal, default: 0, precision: 10, scale: 2
      change_column :invoice_headers, :total, :decimal, null: false, default: 0, precision: 10, scale: 2
    
  end
end	
