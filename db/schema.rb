# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20201213131911) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "invoice_headers", force: :cascade do |t|
    t.text     "name",                                                null: false
    t.text     "lastname",                                            null: false
    t.string   "dni",                                                 null: false
    t.text     "adress"
    t.string   "phone"
    t.string   "email"
    t.decimal  "tax",        precision: 10, scale: 2, default: "0.0"
    t.decimal  "total",      precision: 10, scale: 2, default: "0.0", null: false
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  create_table "invoice_lines", force: :cascade do |t|
    t.text     "name"
    t.integer  "quantity",          null: false
    t.decimal  "unit_price",        null: false
    t.string   "color"
    t.decimal  "line_total"
    t.integer  "invoice_header_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["invoice_header_id"], name: "index_invoice_lines_on_invoice_header_id", using: :btree
  end

  create_table "invoices", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "invoice_lines", "invoice_headers"
end
