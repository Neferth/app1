class InvoiceHeader < ApplicationRecord
	has_many :invoice_lines, dependent: :destroy

	validates_presence_of :dni

	def get_fullname
  		[name, lastname].select(&:present?).join(' ').titleize
	end

	def get_numlines
		invoice_lines.count
	end

	def update_total
		self.total = invoice_lines.map{|i| i.line_total }.sum * 1.21
		self.tax = invoice_lines.map{|i| i.line_total }.sum * 0.21
		self.save
	end

end
