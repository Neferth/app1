class InvoiceLine < ApplicationRecord
  belongs_to :invoice_header
  before_save  :set_line_total
  after_save :update_total

  
  def set_line_total
  	self.line_total = self.quantity * self.unit_price
  end

  def update_total
  	self.invoice_header.update_total  	
  end
  
end
