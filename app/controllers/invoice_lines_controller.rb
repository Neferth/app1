class InvoiceLinesController < ApplicationController

	def new
	end

	def create		
		@in = InvoiceHeader.find(params[:invoice_line][:invoice_header_id])
		@line=@in.invoice_lines.create(params_line)
		@num_lin = @in.get_numlines
		redirect_to invoice_header_path(@in.id)
	end

	def destroy
	    @line = InvoiceLine.find(params[:id])
	    @invoice = @line.invoice_header
	    @line.destroy
	    @invoice.update_total
	    redirect_to invoice_header_path(@invoice.id)
	end

	private

		def params_line
			params.require(:invoice_line).permit(:invoice_header_id, :id, :name, :quantity, :unit_price, :color)
		end
end


