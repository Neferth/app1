class InvoiceHeadersController < ApplicationController

	def new
	end

	def create
		@invoice = InvoiceHeader.create(params_invoice)
		redirect_to invoice_header_path(id: @invoice.id)
	end

	def edit
	end

	def update
		@invoice = InvoiceHeader.find(params[:id])
		@invoice.update(params_invoice)
		redirect_to invoice_header_path(id: @invoice.id)
	end

	def show
		@invoice = InvoiceHeader.find(params[:id])
		respond_to do |format|
	        format.html     
	        format.pdf do
	        	render template: 'invoice_headers/invoice_template.html.slim', pdf: 'invoice' + Time.now.strftime('%v %H:%M:%S').to_s, javascript_delay: 10000,
   				layout: 'pdf_layout.html.slim', disposition: (params[:mode])
	        end
	    end
    end
	

	def index
		@invoices = InvoiceHeader.all 
	end
	def destroy
	    @invoice = InvoiceHeader.find(params[:id])
	    @invoice.destroy
	    redirect_to invoice_headers_path()
	end


	private

		def params_invoice
			params.require(:invoice_header).permit(:id, :name, :lastname, :dni, :adress, :phone, :email)
		end


end
